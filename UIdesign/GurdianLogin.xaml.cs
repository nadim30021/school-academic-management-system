﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for GurdianLogin.xaml
    /// </summary>
    public partial class GurdianLogin : Window
    {
        public GurdianLogin()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            m.Show();
            this.Close();
        }

        private void showInfo_Click(object sender, RoutedEventArgs e)
        {
            ClassSchedule c = new ClassSchedule();
            c.Show();
            this.Close();
        }

        private void perfomance_Click(object sender, RoutedEventArgs e)
        {
            AccountsInfo acc = new AccountsInfo();
            acc.Show();
            this.Close();
        }

        private void classShedule_Click(object sender, RoutedEventArgs e)
        {
            GradeReportGuardian g = new GradeReportGuardian();
            g.Show();
            this.Close();
        }

        private void documentShare_Click(object sender, RoutedEventArgs e)
        {
            MeetingNotice m = new MeetingNotice();
            m.Show();
            this.Close();

            
            

            
           

            
        }
    }
}
