﻿#pragma checksum "..\..\StudentLogin.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F077B699969B94846FE49F6F9BD7E30D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace UIdesign {
    
    
    /// <summary>
    /// StudentLogin
    /// </summary>
    public partial class StudentLogin : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button showInfo;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button perfomance;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button classShedule;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button documentShare;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button notice;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button reg;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\StudentLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label stlogin;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/UIdesign;component/studentlogin.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\StudentLogin.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.showInfo = ((System.Windows.Controls.Button)(target));
            
            #line 10 "..\..\StudentLogin.xaml"
            this.showInfo.Click += new System.Windows.RoutedEventHandler(this.showInfo_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.perfomance = ((System.Windows.Controls.Button)(target));
            
            #line 11 "..\..\StudentLogin.xaml"
            this.perfomance.Click += new System.Windows.RoutedEventHandler(this.perfomance_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.classShedule = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\StudentLogin.xaml"
            this.classShedule.Click += new System.Windows.RoutedEventHandler(this.classShedule_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.documentShare = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\StudentLogin.xaml"
            this.documentShare.Click += new System.Windows.RoutedEventHandler(this.documentShare_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.notice = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\StudentLogin.xaml"
            this.notice.Click += new System.Windows.RoutedEventHandler(this.notice_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.reg = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\StudentLogin.xaml"
            this.reg.Click += new System.Windows.RoutedEventHandler(this.reg_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 17 "..\..\StudentLogin.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.stlogin = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

