﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for ShowAssignSubject.xaml
    /// </summary>
    public partial class ShowAssignSubject : Window
    {

        string tea="";
        
        public ShowAssignSubject()
        {
            InitializeComponent();
        }


        public ShowAssignSubject(string t)
        {
            InitializeComponent();

            tea = t;
            List<assign> users = new List<assign>();

            ShowAssignSubjectClass sd = new ShowAssignSubjectClass(tea);
            users = sd.show();

            ShAssSubGrd.ItemsSource = users;

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Login i = new Login();
            i.Show();
            this.Close();
        }

        
    }
}
