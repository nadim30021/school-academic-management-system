﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for ShowTimeTable.xaml
    /// </summary>
    public partial class ShowTimeTable : Window
    {

        string tea;
       
        public ShowTimeTable()
        {
            InitializeComponent();
        }


        public ShowTimeTable(string t)
        {
            tea = t;

            InitializeComponent();
            ShowTimeTableClass sd = new ShowTimeTableClass(tea);
            
            
            List<time> users = new List<time>();

           
            users = sd.show();

            ShTimTabGrd.ItemsSource = users;
        }

        

/*
        public ShowTimeTable(string t)
        {
            tea = t;


            


        }*/

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            TeacherLogin i = new TeacherLogin();
            i.Show();
            this.Close();
        }

        
    }
}
