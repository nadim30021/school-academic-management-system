﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for AccountsInfo.xaml
    /// </summary>
    public partial class AccountsInfo : Window
    {
        public AccountsInfo()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            GurdianLogin l = new GurdianLogin();
            l.Show();
            this.Close();
        }

      


        private void accountInfoView_Click(object sender, RoutedEventArgs e)
        {

            try
            {
            projectLinqDataContext prd = new projectLinqDataContext(MyDB.ConnectionString);

            Accounts_Info accountsinfo = new Accounts_Info();

            string i = acountInfoviewTxt.Text;

                var info = from k in prd.Accounts_Infos
                           where k.Student_Id == i
                           select k;
                
                accountInfoGrid.ItemsSource = info;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Student ID not Valid");
            }


        }
    }
}
