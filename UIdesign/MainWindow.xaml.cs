﻿using MahApps.Metro.Controls;
using System.Windows;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Login win2 = new Login();
            win2.getuser("Admin");
            win2.Show();
            this.Close();
        }

        private void teacher_Click(object sender, RoutedEventArgs e)
        {
            Login win2 = new Login();
            win2.getuser("Teacher");
            win2.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Login win2 = new Login();
            win2.getuser("Student");
            win2.Show();
            this.Close();
        }

        private void guardian_Click(object sender, RoutedEventArgs e)
        {
            Login win2 = new Login();
            win2.getuser("Guardian");
            win2.Show();
            this.Close();
        }

       

       
    }
}
