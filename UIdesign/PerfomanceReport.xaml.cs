﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for PerfomanceReport.xaml
    /// </summary>
    public partial class PerfomanceReport : Window
    {

        string st = "";

        public PerfomanceReport()
        {
            
        }

        public PerfomanceReport(string t)
        {
            InitializeComponent();
            st = t;

            projectLinqDataContext prd = new projectLinqDataContext(MyDB.ConnectionString);

            Grade grd = new Grade();

            

            var info = from k in prd.Grades
                       where k.Student_Id == st
                       select k;

            prfgrid.ItemsSource = info;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            StudentLogin s = new StudentLogin();
            s.Show();
            this.Close();

        }

        
    }
}
