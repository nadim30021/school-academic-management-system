﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for AssignSubjectToClass.xaml
    /// </summary>
    public partial class AssignSubjectToClass : Window
    {

       
        
        public AssignSubjectToClass()
        {
            InitializeComponent();
        }


        AdminLogin tr = new AdminLogin();
       public void trc(AdminLogin t)
       {
           tr = t;
       }

        

      

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            
             tr.Show();
           this.Close();
        }


       

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {


            try
            {
                projectLinqDataContext prd = new projectLinqDataContext(MyDB.ConnectionString);

                Assign_Subject assiSub = new Assign_Subject();

                assiSub.Teacher_Id = teacher.Text;

                assiSub.Subject_Name = subname.Text;

                assiSub.Subject_Code = subcode.Text;


                prd.Assign_Subjects.InsertOnSubmit(assiSub);

                prd.SubmitChanges();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Subject Already Assign");
            }


            
        }
    }
}
