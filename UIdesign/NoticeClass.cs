﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIdesign
{

    public class notices
    {

        public string Agenda { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

    }

    
    class NoticeClass
    {

        public List<notices> show()
        {
            string sql = "select * from Noticed";
            DataTable dt = DataAccess.GetDataTable(sql);
            // MessageBox.Show(dt.Rows[0]["First Name"].ToString());

            int t = dt.Rows.Count;

            List<notices> users = new List<notices>();

            for (int i = 0; i < t; i++)
            {
                users.Add(new notices() { Agenda = (dt.Rows[i]["Notices"].ToString()), Date = (dt.Rows[i]["Date"].ToString()), Time = (dt.Rows[i]["Time"].ToString()) });

            }

            

            return users;
        }
    }
}
