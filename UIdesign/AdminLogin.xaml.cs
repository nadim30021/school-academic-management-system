﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for AdminLogin.xaml
    /// </summary>
    public partial class AdminLogin : Window
    {
        public AdminLogin()
        {
            InitializeComponent();

        
        }




        public void adgetname(string fn, string ln,string id)
        {
            adminlbl.Content = fn + " " + ln;
            adminIDlbl.Content = id;
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AssignSubjectToClass astc = new AssignSubjectToClass();
            astc.Show();
            astc.trc(this);
            this.Hide();

            

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            VerifyRegistration vr = new VerifyRegistration();
            vr.Show();
            vr.trc(this);
            this.Hide();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            GradePublish gp = new GradePublish();
           
            gp.Show();
            gp.trc(this);
            this.Hide();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            GradeFinalize gf = new GradeFinalize();
           
            gf.Show();
            gf.trc(this);
            this.Hide();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            CallForMeeting cfm = new CallForMeeting();
            cfm.trc(this);
            cfm.Show();
            cfm.trc(this);
            this.Hide();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            m.Show();
            this.Close();
        }
    }
}
