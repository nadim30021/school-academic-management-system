﻿using System;
using System.Windows;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for StudentLogin.xaml
    /// </summary>
    public partial class StudentLogin : Window
    {
        string st="";
        
        public StudentLogin()
        {
            InitializeComponent();
           
        }


        public StudentLogin(string t)
        {
            InitializeComponent();
            stlogin.Content = t;
            st = t;
        }

       

        private void showInfo_Click(object sender, RoutedEventArgs e)
        {
            ShowInfo sif = new ShowInfo(st);
            sif.Show();
           
            this.Close();
        }

        private void perfomance_Click(object sender, RoutedEventArgs e)
        {
            PerfomanceReport perfrprt = new PerfomanceReport(st);
            perfrprt.Show();
            this.Close();
        }

        private void classShedule_Click(object sender, RoutedEventArgs e)
        {
            ClsScheduleStd clshed = new ClsScheduleStd(st);
            clshed.Show();
            this.Close();
        }

        private void documentShare_Click(object sender, RoutedEventArgs e)
        {
            DocumentSharing docshare = new DocumentSharing(st);
            docshare.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow i = new MainWindow();
            i.Show();
            this.Close();
        }

        private void notice_Click(object sender, RoutedEventArgs e)
        {
            Notice n = new Notice();
            n.Show();
            this.Close();
        }

        private void reg_Click(object sender, RoutedEventArgs e)
        {
            Registretion rg = new Registretion();
            rg.Show();
            this.Hide();
        }

        internal void getID()
        {
            throw new NotImplementedException();
        }
    }
}
