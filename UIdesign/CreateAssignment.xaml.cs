﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for CreateAssignment.xaml
    /// </summary>
    public partial class CreateAssignment : Window
    {
        public CreateAssignment()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension

            dlg.DefaultExt = ".txt";

            dlg.Filter = "Text documents (.txt)|*.txt";



            // Display OpenFileDialog by calling ShowDialog method

            Nullable<bool> result = dlg.ShowDialog();



            // Get the selected file name and display in a TextBox

            if (result == true)
            {

                // Open document

                string filename = dlg.FileName;

                FileNameTextBox.Text = filename;

            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow i = new MainWindow();
            i.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string sql = "insert into Create_Assignment values('" + title1.Text + "','" + numb1.Text + "','" + FileNameTextBox.Text + "') ";
            DataAccess.ExecuteSQL(sql);
            MessageBox.Show("Assignment Uploaded");
            FileNameTextBox.Text = "";
            title1.Text = "";
            numb1.Text = "";

        }

        
    }
}
