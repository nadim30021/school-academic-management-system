﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for VerifyRegistration.xaml
    /// </summary>
    public partial class VerifyRegistration : Window
    {

        AdminLogin tr = new AdminLogin();
        public void trc(AdminLogin t)
        {
            tr = t;
        }
        
        public VerifyRegistration()
        {
            InitializeComponent();
        }

       

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            tr.Show();
            this.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
                string tx1 = StdIdlbl.Text.ToString();
                
                string sql = "select * from Student where Id='" + tx1 + "'";
                DataTable dt = DataAccess.GetDataTable(sql);

                string x = dt.Rows[0]["Id"].ToString();
                

                if (tx1 == x)
                {
                    MessageBox.Show("Student Verified");
                }

                // string tx2 = Upgrade.Text.ToString();

                // string db1 = dt.Rows[i]["Id"].ToString();
                // string db2 = dt.Rows[i]["Password"].ToString();


            }
            catch (Exception ex)
            {
                // throw ex;
                MessageBox.Show("Invalid Student");
            }
            StdIdlbl.Text = "";
            StdNamelbl.Text = "";
            Upgrade.Text = "";
            Gpa.Text = "";
           
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            CallForMeeting c = new CallForMeeting();
            c.Show();
            this.Close();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            GradeFinalize gf = new GradeFinalize();
            gf.Show();
            this.Close();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            AssignSubjectToClass a = new AssignSubjectToClass();
            a.Show();
            this.Close();
        }
    }
}
