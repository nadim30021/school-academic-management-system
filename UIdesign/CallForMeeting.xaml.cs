﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for CallForMeeting.xaml
    /// </summary>
    public partial class CallForMeeting : Window
    {
        public CallForMeeting()
        {
            InitializeComponent();
        }


        AdminLogin tr = new AdminLogin();
        public void trc(AdminLogin t)
        {
            tr = t;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            tr.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {


            projectLinqDataContext prd = new projectLinqDataContext(MyDB.ConnectionString);

            Noticed notice = new Noticed();

            notice.NoticeId = ntcnmbr.Text;
            notice.Time = time.Text;
            notice.Notices = agenda.Text;
            string s = date.SelectedDate.Value.Date.ToShortDateString();

            notice.Date = s;


            prd.Noticeds.InsertOnSubmit(notice);

            prd.SubmitChanges();
            
       
        }

     
    }
}
