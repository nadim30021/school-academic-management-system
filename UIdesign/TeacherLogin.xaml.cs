﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for TeacherLogin.xaml
    /// </summary>
    public partial class TeacherLogin : Window
    {

        string tea="";
        
        public TeacherLogin()
        {
            InitializeComponent();
        }

        public TeacherLogin(string t)
        {
            InitializeComponent();
            tea=t;
        }


        public void tegetname(string fn, string ln, string id)
        {
            tenamelbl.Content = fn + " " + ln;
            teIDlbl.Content = id;
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ManageGroup mg = new ManageGroup();
            mg.Show();
            this.Close();
        }

        private void ShowTimeTable_Click(object sender, RoutedEventArgs e)
        {
            ShowTimeTable stt = new ShowTimeTable(tea);
           
            stt.Show();
            this.Close();

        }

        private void TakeAttandance_Click(object sender, RoutedEventArgs e)
        {
            TakeAttandance tat = new TakeAttandance();
            tat.Show();
            this.Close();
        }

        private void ShowAssignSubject_Click(object sender, RoutedEventArgs e)
        {
            ShowAssignSubject sas = new ShowAssignSubject(tea);
            sas.Show();
            this.Close();
        }

        private void LessonePlanning_Click(object sender, RoutedEventArgs e)
        {
            LessonPlanning lp = new LessonPlanning();
            lp.Show();
            this.Close();

        }

        private void CreateAssignment_Click(object sender, RoutedEventArgs e)
        {
            CreateAssignment cra = new CreateAssignment();
            cra.Show();
            this.Close();
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow i = new MainWindow();
            i.Show();
            this.Close();
        }
    }
}
