﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for Registretion.xaml
    /// </summary>
    public partial class Registretion : Window
    {
        public Registretion()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            StudentLogin s = new StudentLogin();
            s.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string sql = "insert into registration values('" + id1.Text + "','" + name1.Text + "','" + reg1.Text + "')";
            DataAccess.ExecuteSQL(sql);
            MessageBox.Show("You are now registered for the selected class");

            id1.Text = "";
            name1.Text = "";
            reg1.Text = "";
        }
    }
}
