﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for TakeAttandance.xaml
    /// </summary>
    public partial class TakeAttandance : Window
    {
        public TakeAttandance()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            TeacherLogin i = new TeacherLogin();
            i.Show();
            this.Close();
        }

        

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            string t = dt1.Content.ToString();
            t = t.Replace("-", "_");

            string sql = "ALTER TABLE Attandance ADD _" + t + " nvarchar(50)";
            DataAccess.ExecuteSQL(sql);


            List<attand> users = new List<attand>();

            TakeAttandanceClass sd = new TakeAttandanceClass("_" + t, classatt.Text, secTxt.Text);
            users = sd.show();

            AttGrid.ItemsSource = users;
        }

        private void Done_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
