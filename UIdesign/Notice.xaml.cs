﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for Notice.xaml
    /// </summary>
    public partial class Notice : Window
    {
        public Notice()
        {
            InitializeComponent();

            List<notices> users = new List<notices>();

            NoticeClass mnt = new NoticeClass();

            users = mnt.show();


            NoticeGrid.ItemsSource = users;

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            StudentLogin s = new StudentLogin();
            s.Show();
            this.Close();

        }

        
    }
}
