﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UIdesign
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    /// 
    ///ResizeMode="NoResize"
    public partial class Login : Window
    {
        string user;
        string stu="";

        public Login()
        {
            InitializeComponent();
        }
        public void getuser(string str)
        {
            user = str;
        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (user == "Admin")
            {

                string sql = "select * from Admin";
                DataTable dt = DataAccess.GetDataTable(sql);

                int t = dt.Rows.Count;

                for (int i = 0; i < t; i++)
                {

                    //Console.WriteLine(userid.Text + " " + password.Password);


                    string tx1 = userid.Text.ToString();
                    string tx2 = password.Password.ToString();

                    string db1 = dt.Rows[i]["Id"].ToString();
                    string db2 = dt.Rows[i]["Password"].ToString();

                    Console.WriteLine(tx1 + " " + tx2);
                    Console.WriteLine(db1 + " " + db2);

                    if (tx1 == db1 && tx2 == db2)
                    {
                        AdminLogin adl = new AdminLogin();
                        adl.Show();
                     
                        this.Close();
                        adl.adgetname((dt.Rows[i]["First Name"].ToString()), (dt.Rows[i]["Last Name"].ToString()), (dt.Rows[i]["Id"].ToString()));

                        //  Console.WriteLine("HEllo");
                        break;

                    }
                    else
                    {
                        wrong w = new wrong();
                        w.Show();
                        this.Close();
                    }
                }

            }





            else if (user == "Teacher")
            {

                string sql = "select * from Teacher";
                DataTable dt = DataAccess.GetDataTable(sql);

                int t = dt.Rows.Count;

                for (int i = 0; i < t; i++)
                {

                    //Console.WriteLine(userid.Text + " " + password.Password);


                    string tx1 = userid.Text.ToString();
                    string tx2 = password.Password.ToString();

                    string db1 = dt.Rows[i]["Id"].ToString();
                    string db2 = dt.Rows[i]["Password"].ToString();

                    Console.WriteLine(tx1 + " " + tx2);
                    Console.WriteLine(db1 + " " + db2);

                    if (tx1 == db1 && tx2 == db2)
                    {
                        TeacherLogin adl = new TeacherLogin(tx1);
                        adl.Show();
                        
                        this.Close();

                        adl.tegetname((dt.Rows[i]["First Name"].ToString()), (dt.Rows[i]["Last Name"].ToString()), (dt.Rows[i]["Id"].ToString()));

                        //  Console.WriteLine("HEllo");
                        break;

                    }
                    else
                    {
                        wrong w = new wrong();
                        w.Show();
                        this.Close();
                    }
                  
                }
            }

            else if (user == "Student")
            {

                string sql = "select * from Student";
                DataTable dt = DataAccess.GetDataTable(sql);

                int t = dt.Rows.Count;
               // MessageBox.Show(t.ToString());

                for (int i = 0; i < t; i++)
                {

                    //Console.WriteLine(userid.Text + " " + password.Password);


                    string tx1 = userid.Text.ToString();
                    stu = tx1;
                    string tx2 = password.Password.ToString();

                    string db1 = dt.Rows[i]["Id"].ToString();
                    string db2 = dt.Rows[i]["Password"].ToString();

                    Console.WriteLine(tx1 + " " + tx2);
                    Console.WriteLine(db1 + " " + db2);

                    if (tx1 == db1 && tx2 == db2)
                    {
                        
                        
                        StudentLogin adl = new StudentLogin(tx1);
                        
                        adl.Show();
                        this.Close();
                        
                        

                        //  Console.WriteLine("HEllo");
                        break;

                    }
                    else
                    {
                        wrong w = new wrong();
                        w.Show();
                        this.Close();
                    }

                }
            }
            else if (user == "Guardian")
            {

                string sql = "select * from Guardian";
                DataTable dt = DataAccess.GetDataTable(sql);

                int t = dt.Rows.Count;

                for (int i = 0; i < t; i++)
                {

                    //Console.WriteLine(userid.Text + " " + password.Password);


                    string tx1 = userid.Text.ToString();
                    string tx2 = password.Password.ToString();

                    string db1 = dt.Rows[i]["GuardianId"].ToString();
                    string db2 = dt.Rows[i]["Password"].ToString();

                    Console.WriteLine(tx1 + " " + tx2);
                    Console.WriteLine(db1 + " " + db2);

                    if (tx1 == db1 && tx2 == db2)
                    {
                        GurdianLogin adl = new GurdianLogin();
                        adl.Show();
                        this.Close();

                        //  Console.WriteLine("HEllo");
                        break;

                    }
                    else
                    {
                        wrong w = new wrong();
                        w.Show();
                        this.Close();
                    }

                }
            }
           
           
        }




        public string getst()
        {
            return stu;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {

        }
    }
}


